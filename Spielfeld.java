import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot und MouseInfo)

/**
 * Ergänzen Sie hier eine Beschreibung für die Klasse Spielfeld.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Spielfeld extends Actor
{
    public Spielfeld() {
        super();
        this.getImage().scale(90,90);
    }
    private int status = 0;
    /**
     * Act - tut, was auch immer Spielfeld tun will. Diese Methode wird aufgerufen, 
     * sobald der 'Act' oder 'Run' Button in der Umgebung angeklickt werden. 
     */
    public void act() 
    {
        // Ergänzen Sie Ihren Quelltext hier...
        if(Greenfoot.mouseClicked(this)) {
            Spielverwaltung verwaltung = ((MyWorld) getWorld()).getSpielverwaltung();
            if(status == 0 && verwaltung.getGameActive()) {
                
                int activePlayer = verwaltung.performTurn();
                move(activePlayer);
                verwaltung.spielendePrüfen((MyWorld) getWorld());
            }
        }
    }    
    
    public void move(int spieler) {
        status = spieler;
        if(spieler == 1) {
            setImage("feld1.jpg");
        } else {
            setImage("feld2.jpg");
        }
        this.getImage().scale(90,90);
        
    }
    
    public int getStatus() {
        return status;
    }
}
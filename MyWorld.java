import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot und MouseInfo)

/**
 * Ergänzen Sie hier eine Beschreibung für die Klasse MyWorld.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class MyWorld extends World
{
    private Spielverwaltung spielverwaltung = new Spielverwaltung();
    private Spielfeld[] felder = new Spielfeld[9];
    private final int felderOffsetX = 150;
    private final int felderOffsetY = 150;
    private final int felderDiffX = 100;
    private final int felderDiffY = 100;

    /**
     * Konstruktor für Objekte der Klasse MyWorld
     * 
     */
    public MyWorld()
    {    
        super(500, 600, 1); 
        drawTTT();

        prepare();
    }

    public void drawTTT(){
        this.getBackground().setColor(Color.BLACK);
        //this.getBackground().drawLine(100,100,400,100);
        this.getBackground().drawLine(100,200,400,200);
        this.getBackground().drawLine(100,300,400,300);
        this.getBackground().drawLine(200,100,200,400);
        this.getBackground().drawLine(300,100,300,400);
    }

    /**
     * Bereite die Welt für den Programmstart vor.
     * Das heißt: Erzeuge die Anfangs-Objekte und füge sie der Welt hinzu.
     */
    private void prepare()
    {
        for (int i = 0; i < 9; i++) {
            felder[i] = new Spielfeld();
            addObject(felder[i], felderOffsetX + (i%3)*felderDiffX, felderOffsetY + (i/3)*felderDiffY);
        }
    }
    
    public void draw() {
        showText("Unentschieden!", getWidth()/2,50);
    }
    
    public void win(int player) {
        showText("Spieler " + player + " hat gewonnen!", getWidth()/2,50);
    }

    public Spielverwaltung getSpielverwaltung() {
        return spielverwaltung;
    }
    
    public Spielfeld[] getFelder() {
        return felder;
    }
}
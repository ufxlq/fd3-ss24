/**
 * Ergänzen Sie hier eine Beschreibung für die Klasse Spielerverwaltung.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Spielverwaltung  
{
    // Attribute -  ersetzen Sie das Beispiel hier mit ihren eigenen Attributen
    private int activePlayer = 1;
    private int gameStatus = 0;
    private final int[][] lines = new int[][] {
            new int[] {0, 1, 2},
            new int[] {3,4,5},
            new int[] {6,7,8},
            new int[] {0,3,6},
            new int[] {1,4,7},
            new int[] {2,5,8},
            new int[] {0,4,8},
            new int[] {2,4,6}
        };

    /**
     * Konstruktor für Objekte der Klasse Spielerverwaltung
     */
    public Spielverwaltung()
    {
    }

    /**
     * Ein Beispiel für eine Methode - ersetzen Sie diesen
     * Kommentar durch Ihren eigenen.
     * 
     * @param  y   ein Beispiel-Parameter für eine Methode
     * @return     irgendeine Zahl
     */
    public int performTurn()
    {
        int turn = activePlayer;
        if (activePlayer == 1) {
            activePlayer = 2;
        } else {
            activePlayer = 1;
        }
        // Ergänzen Sie Ihren Quelltext hier...
        return turn;
    }

    public void spielendePrüfen(MyWorld world) {
        Spielfeld[] felder = world.getFelder();
        boolean draw = true;
        for (int i = 0; i < 9 && draw; i++) {
            if (felder[i].getStatus() == 0) {
                draw = false;
            }
        }
        if (draw) {
            gameStatus = -1;
            world.draw();
            return;
        }
        for (int i = 0; i < lines.length; i++) {
            int[] line = lines[i];
            int a = line[0];
            int b = line[1];
            int c = line[2];
            if (felder[a].getStatus() != 0 && felder[a].getStatus() == felder[b].getStatus() && felder[b].getStatus() == felder[c].getStatus()) {
                gameStatus = felder[a].getStatus();
                world.win(gameStatus);
                return;
            }
        }
    }
    public boolean getGameActive() {
        return gameStatus == 0;
    }
}